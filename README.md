# Mini-Projet : Simulations de feux de foret

## Authors
Victor Jacoillot & Charles Berdugo & Jeremy Dufosse

## Date
20/12/2019

## Commande programme
Exemple : ``` Program.py -rows=10 -cols=10 -cell_size=35 -duration=100 -afforestation=0.6 ```