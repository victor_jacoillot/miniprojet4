# /usr/bin/env python3
# coding: utf-8

import numpy as np
from tkinter import *
from random import random
import argparse


# Calculer et dessiner la prochaine génération
def iteration(p):
    appliquer_regles(p)
    dessiner()

def iteration_boucle(p):
    global flag
    appliquer_regles(p)
    dessiner()
    if flag==1:
        fenetre.after(timer, iteration_boucle, p)
    else:
        flag=0

# Initialisation de l'automate
def initialiser_monde(p):
    # Répartition aléatoire sur la grille des arbres
    etat[0:NbL,0:NbC] = state["VIDE"]
    for x in range(NbL):
        for y in range(NbC):
            if random() < p:
                etat[x,y] = state["ARBRE"]
    # Création de la grille d'affichage
    for x in range(NbL):
        for y in range(NbC):
            if etat[x,y]==state["VIDE"]:
                couleur = Couleur_VIDE
            if etat[x,y]==state["ARBRE"]:
                couleur = Couleur_ARBRE
            cell[x,y] = canvas.create_rectangle((x*cellule, y*cellule, (x+1)*cellule, (y+1)*cellule), outline="black", fill=couleur)

# Calcul de la prochaine génération
def appliquer_regles(p):
    global etat
    temp = etat.copy()  # sauvegarde de l'état courant
    for x in range(NbL):
        for y in range(NbC):
            if etat[x,y] == state["FEU"]:
                temp[(x)%NbL,(y)%NbC] = state["CENDRE"]
                # 4 voisins
                if etat[x,(y+1)%NbC] == state["ARBRE"]:           # côté haut
                    if random() < p:
                        temp[x,(y+1)%NbC] = state["FEU"]
                if etat[(x-1)%NbL,y] == state["ARBRE"]:           # côté gauche
                    if random() < p:
                        temp[(x-1)%NbL,y] = state["FEU"]
                if etat[(x+1)%NbL,y] == state["ARBRE"]:           # côté droit
                    if random() < p:
                        temp[(x+1)%NbL,y] = state["FEU"]
                if etat[x,(y-1)%NbC] == state["ARBRE"]:           # côté bas
                    if random() < p:
                        temp[x,(y-1)%NbC] = state["FEU"]

                # 8 voisins
#                if etat[(x-1)%NbL,(y+1)%NbC] == state["ARBRE"]:   # diagonale haut gauche
#                    if random() < p:
#                        temp[(x-1)%NbL,(y+1)%NbC] = state["FEU"]
#                if etat[(x+1)%NbL,(y+1)%NbC] == state["ARBRE"]:   # diagonale haut droit
#                    if random() < p:
#                        temp[(x+1)%NbL,(y+1)%NbC] = state["FEU"]
#                if etat[(x-1)%NbL,(y-1)%NbC] == state["ARBRE"]:   # diagonale bas gauche
#                    if random() < p:
#                        temp[(x-1)%NbL,(y-1)%NbC] = state["FEU"]
#                if etat[(x+1)%NbL,(y-1)%NbC] == state["ARBRE"]:   # diagonale bas droit
#                    if random() < p:
#                        temp[(x+1)%NbL,(y-1)%NbC] = state["FEU"]

            if etat[x,y] == state["CENDRE"]:
                temp[(x)%NbL,(y)%NbC] = state["VIDE"]
    etat = temp.copy()  # maj de l'état courant

# Dessiner toutes les cellules
def dessiner():
    for x in range(NbL):
        for y in range(NbC):
            if etat[x,y]==state["ARBRE"]:
                couleur = Couleur_ARBRE
            if etat[x,y]==state["VIDE"]:
                couleur = Couleur_VIDE
            if etat[x,y]==state["FEU"]:
                couleur = Couleur_FEU
            if etat[x,y]==state["CENDRE"]:
                couleur = Couleur_CENDRE
            canvas.itemconfig(cell[x][y], fill=couleur)

# Arrêt de l'animation
def stop():
    global flag
    flag=0

# Démarrage de l'animation
def start():
    global flag
    if flag==0:
        flag=1
    iteration_boucle(ProbaBruler)

# Animation pas à pas
def pasapas():
    global flag
    flag=0
    iteration(ProbaBruler)

# Réinitialisation de la grille
def resetButton():
    global flag
    flag=0
    initialiser_monde(DensiteArbre)

# Fonction de traitement du clic gauche de la souris
def click_gauche(event):
    x, y = event.x//cellule, event.y//cellule
    if etat[x,y] == state["ARBRE"]:
        etat[x,y] = state["FEU"]
        canvas.itemconfig(cell[x][y], fill=Couleur_FEU)

# Fonction de traitement du clic droit de la souris
def click_droit(event):
    x, y = event.x//cellule, event.y//cellule
    if etat[x,y] == state["FEU"]:
        etat[x,y] = state["ARBRE"]
        canvas.itemconfig(cell[x][y], fill=Couleur_ARBRE)

# Fonction d'aide pour les arguments
def argshelp():
    parser = argparse.ArgumentParser()
    parser.add_argument('-rows', help = "number of rows (int)", type = int)
    parser.add_argument("-cols", help = "number of columns (int)", type = int)
    parser.add_argument("-cell_size", help = "size of one cell (int)", type = int)
    parser.add_argument("-duration", help = "number of secondes (int)", type = int)
    parser.add_argument("-afforestation", help = "probability of a cell being a tree between 0 and 1 (float)", type = float)
    args = parser.parse_args()
    return args

# Définition du nombre de lignes
def rows(args):
    rows = 0
    if(args.rows is not None):
        if(args.rows < 1):
            rows = 5
        else:
            rows = args.rows
    return rows

# Définition du nombre de colonnes
def cols(args):
    cols = 0
    if(args.cols is not None):
        if(args.cols < 1 ):
            cols = 5
        else:
            cols = args.cols
    return cols

# Définition de la taille des cellules
def cell_size(args):
    cell_size = 0
    if(args.cell_size is not None):
        if(args.cell_size < 1):
            cell_size = 1
        else:
            cell_size = args.cell_size
    return cell_size

# Définition du taux d'afforestation
def afforestation(args):
    afforestation = 0.0
    if(args.afforestation is not None):
        args.afforestation = float(args.afforestation)
        if(args.afforestation < 0 or args.afforestation > 1):
            afforestation = 0.6
        else :
            afforestation = args.afforestation
    return afforestation

# Définition de la vitesse d'animation
def duration(args):
    duration = 0
    if(args.duration is not None):
        if(args.duration < 99):
            duration = 100
        else :
            duration = args.duration
    return duration


if __name__ == "__main__":

    args = argshelp()
    NbL = rows(args)                    # hauteur du tableau (Lignes)
    NbC = cols(args)                    # largeur du tableau (Colonnes)
    cellule = cell_size(args)           # taille d'une cellule
    timer = duration(args)              # temps en millisecondes (ms)
    DensiteArbre = afforestation(args)  # densité d'arbres
    ProbaBruler = 1.0                   # probabilité que le feu brûle les arbres   

    # Définition des matrices d'évolution de l'automate
    cell = np.zeros((NbL,NbC),dtype=int)
    etat = np.zeros((NbL,NbC),dtype=int)

    # Définition de l'alphabet de l'automate dans un dictionnaire Python
    state = {"ARBRE":0,"FEU":1,"VIDE":2,"CENDRE":3}
    # Définition des couleurs associées
    Couleur_ARBRE = "green"
    Couleur_VIDE = "white"
    Couleur_FEU = "red"
    Couleur_CENDRE = "gray"

    # Définition de l'interface graphique
    fenetre = Tk()
    fenetre.title("Simulation de la propagation d'un feu de fôret")
    canvas = Canvas(fenetre, width=cellule*NbC+1, height=cellule*NbL+1, highlightthickness=0)
    fenetre.wm_attributes("-topmost", True)
    fenetre.minsize(width=cellule*NbC+40, height=cellule*NbL+40)

    # Allocation des fonctions click souris
    canvas.bind("<Button-1>", click_gauche)
    canvas.bind("<Button-3>", click_droit)
    canvas.pack()

    # Définition des boutons de commande
    btn_1 = Button(fenetre,text='Fermer la fenêtre', width=13, command=fenetre.destroy)
    btn_1.pack(side=RIGHT)
    btn_2 = Button(fenetre, text='Commencer', width=10, command=start)
    btn_2.pack(side=LEFT)
    btn_3 = Button(fenetre, text='Pas à Pas', width=9, command=pasapas)
    btn_3.pack(side=LEFT)
    btn_4 = Button(fenetre, text='Arrêt', width=8, command=stop)
    btn_4.pack(side=LEFT)
    btn_5 = Button(fenetre, text='Réinitialisation', width=11, command=resetButton)
    btn_5.pack(side=LEFT)

    # Lancement de l'automate
    flag = 0
    initialiser_monde(DensiteArbre)
    fenetre.mainloop()